import { Component } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent {
  display: string = '';
  operand1: string = '';
  operand2: string = '';
  operation: string = '';

  appendToDisplay(number: string) {
    if (this.operation === '') {
      this.operand1 += number;
      this.display = this.operand1;
    } else {
      this.operand2 += number;
      this.display = this.operand2;
    }
  }

  performOperation(op: string) {
    this.operation = op;
  }

  clearDisplay() {
    this.operand1 = '';
    this.operand2 = '';
    this.operation = '';
    this.display = '';
  }

  calculateResult() {
    if (this.operand1 !== '' && this.operand2 !== '') {
      const num1 = parseFloat(this.operand1);
      const num2 = parseFloat(this.operand2);
      let result = 0;
      switch (this.operation) {
        case '+':
          result = num1 + num2;
          break;
        case '-':
          result = num1 - num2;
          break;
        case '*':
          result = num1 * num2;
          break;
        case '/':
          result = num1 / num2;
          break;
      }
      this.display = result.toString();
      this.operand1 = result.toString();
      this.operand2 = '';
      this.operation = '';
    }
  }
}
